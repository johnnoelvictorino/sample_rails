class Scholar < ApplicationRecord
  has_one :user, as: :user, dependent: :destroy
  accepts_nested_attributes_for :user
end