ActiveAdmin.register User do
  menu parent: "Users"
	
	controller do
    def permitted_params
      params.permit!
    end
  end

  index do
  	selectable_column
  	id_column
  	column :email
  	column :password
  	actions
  end

	form do |f|
		f.inputs do
			f.input :email
			f.input :password
      f.input :password_confirmation
      f.input :user_type, collection: ['Department', 'Scholar']
		end
		f.actions
	end
end
