ActiveAdmin.register Department do
  menu parent: "Users"
	
	controller do
    def permitted_params
      params.permit!
    end
  end

  index do
  	selectable_column
  	id_column
    column :department_name
  	actions
  end

	form do |f|
		f.inputs do
      f.input :department_name

      f.inputs "Account Information", for: [:user, f.object.build_user] do |s|
        s.input :email
        s.input :password
        s.input :password_confirmation

        s.actions
      end

		end
		f.actions
	end
end
