class CreateScholars < ActiveRecord::Migration[5.1]
  def change
    create_table :scholars do |t|
      t.string :last_name
      t.string :first_name
      t.string :middle_initial

      t.timestamps
    end
  end
end
